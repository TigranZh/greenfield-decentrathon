FROM golang:1.20-alpine as builder

RUN apk add --no-cache make git bash build-base linux-headers libc-dev

ADD . /greenfield

ENV CGO_ENABLED=1
ENV GO111MODULE=on

ARG CI_COMMIT_REF_NAME
ARG CI_COMMIT_SHA
ARG CI_COMMIT_TAG


ENV ldflags="-X gitlab.com/TigranZh/greenfield-decentrathon/version.AppVersion=$CI_COMMIT_REF_NAME  \
  -X gitlab.com/TigranZh/greenfield-decentrathon/version.GitCommit=$CI_COMMIT_SHA \
  -X gitlab.com/TigranZh/greenfield-decentrathon/version.GitCommitDate=$(date -u +%Y-%m-%dT%H:%M:%SZ)  \
   -X gitlab.com/TigranZh/greenfield-decentrathon/version.GitTag=$CI_COMMIT_TAG"

RUN cd /greenfield && go build -o build/bin/gnfd -ldflags="$(ldflags)" ./cmd/gnfd/main.go

# Pull greenfield into a second stage deploy alpine container
FROM alpine:3.17

ARG USER=greenfield
ARG USER_UID=1000
ARG USER_GID=1000

ENV CGO_CFLAGS="-O -D__BLST_PORTABLE__"
ENV CGO_CFLAGS_ALLOW="-O -D__BLST_PORTABLE__"

ENV PACKAGES ca-certificates bash curl libstdc++
ENV WORKDIR=/app

RUN apk add --no-cache $PACKAGES \
  && rm -rf /var/cache/apk/* \
  && addgroup -g ${USER_GID} ${USER} \
  && adduser -u ${USER_UID} -G ${USER} --shell /sbin/nologin --no-create-home -D ${USER} \
  && addgroup ${USER} tty \
  && sed -i -e "s/bin\/sh/bin\/bash/" /etc/passwd

RUN echo "[ ! -z \"\$TERM\" -a -r /etc/motd ] && cat /etc/motd" >> /etc/bash/bashrc

WORKDIR ${WORKDIR}

COPY --from=builder /greenfield/build/bin/gnfd ${WORKDIR}/
RUN chown -R ${USER_UID}:${USER_GID} ${WORKDIR}
USER ${USER_UID}:${USER_GID}

EXPOSE 26656 26657 9090 1317 6060 4500

ENTRYPOINT ["/app/gnfd"]
